declare module '*.html' {
    const template: string;
    export default template;
}
//declare function require(string): string;
declare var require: {
    <T>(path: string): T;
    (paths: string[], callback: (...modules: any[]) => void, value: string): void;
    ensure: (paths: string[], callback: (require: <T>(path: string) => T) => void, value: string) => void;
};
//declare function require(string): string;

//const list2 = r => require.ensure([], () => r(require('../list2/list2')),'teste'); funcionou
//const list2 = () => import('../list2/list2'); funcionou
//const TesteMixin = require.ensure([], () => require('../mixin'),'mixins'); mixin

//UsuarioService.then((res) => this.UsuarioService = new res.UsuarioService());