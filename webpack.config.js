const path = require("path");
const webpack = require("webpack");
const {
    TsConfigPathsPlugin
} = require("awesome-typescript-loader");
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

const devMode = 'development' // development | production

const pathPublicLocal = 'http://localhost/public/assets/';
const pathPublicServer = 'http://localhost/public/assets/';
const pathDestino = '../../public/assets';

const entrys = {

    teste: path.resolve(__dirname, "./src/components/teste/bootstrap.ts"),

};

//OTIMIZAÇÃO MODO PRODUÇÃO
const otimizacao = {
    minimizer: [
        new UglifyJsPlugin({
            cache: true,
            parallel: true,
            extractComments: true,
            uglifyOptions: {
                compress: true,
                ecma: 6,
                mangle: false,
                warnings: false,
            },
            sourceMap: true
        }),
        new OptimizeCSSAssetsPlugin({
            cssProcessor: require('cssnano')({
                reduceIdents: false,
                zindex: false
            }),
            cssProcessorOptions: {
                discardComments: {
                    removeAll: true
                }
            },
        })
    ]
}

if (devMode == 'development') {
    const otimizacao = {};
}

module.exports = {

    //ENTRADA
    entry: entrys,

    //SAIDA
    output: {
        publicPath: devMode == 'development' ? pathPublicLocal : pathPublicServer,
        path: path.resolve(__dirname, pathDestino),
        filename: "[name].js",
        chunkFilename: "[name].js"
    },

    //MODULE
    module: {
        rules: [{
            test: /\.html$/,
            loader: "html-loader"
        },
        {
            test: /\.(ttf|eot|woff)$/,
            loader: "file-loader"
        },
        {
            test: /\.(scss|sass|css)$/,
            use: [
                'css-hot-loader',
                MiniCssExtractPlugin.loader,
                'css-loader',
                'sass-loader'
            ]
        },
        {
            test: /\.tsx?$/,
            use: [{
                loader: 'ts-loader',
                options: {
                    transpileOnly: true
                }
            }]
        },
        {
            test: /\.(png|jpg|gif|svg)$/, //|svg
            loader: "file-loader",
            options: {
                name: "[name].[ext]?[hash]"
            }
        },
        {
            test: /\.svg$/, //add svg extension if you want to load raw svg data in component
            use: "raw-loader"
        },
        {
            test: /\.js$/,
            loader: "babel-loader",
            exclude: /node_modules/
        }
        ]
    },

    resolve: {
        extensions: [".js", ".ts", ".json", ".tsx"],
        alias: {
            vue$: "vue/dist/vue.js",
            "jquery-maskmoney": "jquery-maskmoney/dist/jquery.maskMoney.min",
            carousel: "owl.carousel/dist/owl.carousel.min"
        },
        plugins: [new TsConfigPathsPlugin /* { tsconfig, compiler } */()]
    },

    plugins: [

        new ProgressBarPlugin(),

        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery"
        }),

        new MiniCssExtractPlugin({
            filename: "[name].css",
            chunkFilename: "[id].css"
        }),
    ],

    optimization: otimizacao,

    mode: devMode,
    performance: {
        hints: 'warning'
    },
    watchOptions: {
        poll: true
    }
};